#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED

#if __cplusplus <= 199711L
#error must use c++ 11 (option -std=c++11)
#endif

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <chrono>
#include <list>
#include <iomanip>
#include <map>
#include <sstream>

#ifdef __linux__ 
typedef int WORD;
#else
#include <windows.h>
#endif

typedef enum{
#ifdef __linux__ 
	black=30,
	dark_blue=34,
	dark_green=32,
	dark_aqua=36,
	dark_cyan=36,
	dark_red=31,
	dark_purple=35,
	dark_pink=35,
	dark_magenta=35,
	dark_yellow=33,
	dark_white=37,
	gray=90,
	blue=94,
	green=92,
	aqua=96,
	cyan=96,
	red=91,
	purple=95,
	pink=95,
	magenta=95,
	yellow=93,
	white=97
#else
	black=0,
	dark_blue=1,
	dark_green=2,
	dark_aqua=3,
	dark_cyan=3,
	dark_red=4,
	dark_purple=5,
	dark_pink=5,
	dark_magenta=5,
	dark_yellow=6,
	dark_white=7,
	gray=8,
	blue=9,
	green=10,
	aqua=11,
	cyan=11,
	red=12,
	purple=13,
	pink=13,
	magenta=13,
	yellow=14,
	white=15
#endif
}Color;

typedef enum{
    regular=0,
    error=1,
    warning=2,
    debug=3
}Level;

class Log{
public:
    Log(Level lvl,std::string n,std::string msg);
    Level getLevel();
    std::string getTime();
    std::string getName();
    std::string getMessage();
    std::string toString();
protected:
    Level l_level;
    std::string l_time;
    std::string l_name;
    std::string l_message;
    const int nameWidth;
};

class LevelConfig{
public:
    LevelConfig(bool disp,WORD c);
    void setVisibility(bool v);
    void setColor(WORD c);
    bool isVisibility();
    WORD getColor();
protected:
    WORD color;
    bool display;
};

class Logs{
public:
    static void addLog(std::string name,std::string message,Level lvl=regular);
    static void setActivate(Level lvl,bool a);
    static bool isActivate(Level lvl);
    static void enableTerminal(bool u);
    static void enableFile(bool u);
    static void writeCSV(std::string name,std::vector<float> values);
    static void closeCSV(std::string name);
    static void setFileName(std::string name);
    static std::map<Level,LevelConfig> initLevelConfig();
protected:
    static void setColor(WORD c);
    static std::list<Log> logs;
    static bool dispTerm;
    static bool dispFile;
    static std::map<Level,LevelConfig> config;
    static std::map<std::string,std::ofstream*> csvFile;
    static std::string fileName;
    static std::ofstream file;
};

#endif // LOG_H_INCLUDED
