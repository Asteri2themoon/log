#include <vector>

#include "log.h"

std::vector<float> createRow(float x,float y,float z);

int main(){
    Logs::setActivate(debug,true);//activate debug log
    Logs::setFileName("logExample.txt");//can't change name after the first log
    Logs::enableTerminal(true);//enable by default
    Logs::enableFile(true);//disable by default
    Logs::addLog("test_warning","1er message de danger",warning);
    Logs::addLog("test","1er message de test!");
    Logs::addLog("test_debug","debug",debug);
    Logs::addLog("test_erreur","1er message d'erreur",error);
    for(int i=0;i<=100;i++){
        Logs::writeCSV("data.csv",createRow(i,i*i,1.0f/(1+i)));
    }
    Logs::closeCSV("data.csv");
	return 0;
}

std::vector<float> createRow(float x,float y,float z){
    std::vector<float> data;
    data.insert(data.end(),x);
    data.insert(data.end(),y);
    data.insert(data.end(),z);
    return data;
}
