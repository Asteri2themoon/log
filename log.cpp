#include "log.h"

std::list<Log> Logs::logs=std::list<Log>();
bool Logs::dispTerm=true;
bool Logs::dispFile=false;
std::string Logs::fileName="log.txt";
std::map<Level,LevelConfig> Logs::config=Logs::initLevelConfig();
std::ofstream Logs::file;
std::map<std::string,std::ofstream*> Logs::csvFile=std::map<std::string,std::ofstream*>();

Log::Log(Level lvl,std::string n,std::string msg) : nameWidth(15){
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);
	std::stringstream ss;
#ifdef __linux__
	ss << std::string(std::asctime(localtime(&in_time_t))).substr(11,8);
#else
	ss << std::put_time(std::localtime(&in_time_t), "%H:%M:%S");
#endif

	l_time=ss.str();
	l_level=lvl;
	l_name=n;
	l_message=msg;
}
Level Log::getLevel(){
	return l_level;
}
std::string Log::getTime(){
	return l_time;
}
std::string Log::getName(){
	return l_name;
}
std::string Log::getMessage(){
	return l_message;
}
std::string Log::toString(){
	std::stringstream ss;
	ss << "[" << l_time << "]" << std::setw(nameWidth) << l_name << ": " << l_message;
	return ss.str();
}
LevelConfig::LevelConfig(bool disp,WORD c){
	color=c;
	display=disp;
}
void LevelConfig::setVisibility(bool v){
	display=v;
}
void LevelConfig::setColor(WORD c){
	color=c;
}
bool LevelConfig::isVisibility(){
	return display;
}
WORD LevelConfig::getColor(){
	return color;
}

void Logs::addLog(std::string name,std::string message,Level lvl){
	Log log(lvl,name,message);
	Logs::logs.push_front(log);
	LevelConfig lc=Logs::config.at(log.getLevel());
	if(lc.isVisibility()){
		if(Logs::dispTerm){
			Logs::setColor(lc.getColor());
			std::cout << log.toString() << std::endl;
		}
		if(Logs::dispFile){
			if(!Logs::file.is_open()){
				Logs::file.open(Logs::fileName);
			}
			Logs::file << log.toString() << std::endl;
		}
	}
}
void Logs::setFileName(std::string name){
	Logs::fileName=name;
}

bool Logs::isActivate(Level lvl){
	return config.at(lvl).isVisibility();
}

void Logs::setActivate(Level lvl,bool a){
	config.at(lvl).setVisibility(a);
}
void Logs::enableTerminal(bool u){
	dispTerm=u;
}
void Logs::enableFile(bool u){
	dispFile=u;
}
void Logs::writeCSV(std::string name,std::vector<float> values){
	unsigned int i;
	std::ofstream *file;
	std::map<std::string,std::ofstream*>::iterator it=Logs::csvFile.find(name);
	if(it==Logs::csvFile.end()){
		file=new std::ofstream();
		file->open(name);
		Logs::csvFile.insert(std::pair<std::string,std::ofstream*>(name,file));
		it=Logs::csvFile.find(name);
	}else{
		file=(*it).second;
		(*file) << std::endl;
	}
	if(values.size()>0){
		(*file) << values[0];
		for(i=1;i<values.size();i++){
			(*file) << "," << values[i];
		}
	}
}
void Logs::closeCSV(std::string name){
	std::map<std::string,std::ofstream*>::iterator it=Logs::csvFile.find(name);
	std::ofstream *file;
	if(it!=Logs::csvFile.end()){
		file=(*it).second;
		file->close();
		Logs::csvFile.erase(it);
	}
}
std::map<Level,LevelConfig> Logs::initLevelConfig(){
	std::map<Level,LevelConfig> maps;
	LevelConfig reg(true,white);
	LevelConfig err(true,red);
	LevelConfig war(true,yellow);
	LevelConfig deb(false,green);
	maps.insert(std::pair<Level,LevelConfig>(regular,reg));
	maps.insert(std::pair<Level,LevelConfig>(error,err));
	maps.insert(std::pair<Level,LevelConfig>(warning,war));
	maps.insert(std::pair<Level,LevelConfig>(debug,deb));
	return maps;
}

void Logs::setColor(WORD c){
#ifdef __linux__
	std::cout << "\033[" << (int)c << "m";
#else
	HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(hstdout, &csbi);
	SetConsoleTextAttribute(hstdout, c);
#endif
}
